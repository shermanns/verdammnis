macro(GetGitRevision)

find_package(Git)

if(NOT EXISTS ${CMAKE_SOURCE_DIR}/src/versions.hpp)

execute_process(COMMAND ${GIT_EXECUTABLE} rev-parse HEAD
                OUTPUT_VARIABLE MCTDHF_GIT_REVISION
                RESULT_VARIABLE MCTDHF_GET_GIT_REVISION_RESULT
                WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                ERROR_QUIET
                OUTPUT_STRIP_TRAILING_WHITESPACE)

if(NOT ${MCTDHF_GET_GIT_REVISION_RESULT} EQUAL 0)
   set(MCTDHF_GIT_REVISION "unknown")
endif(NOT ${MCTDHF_GET_GIT_REVISION_RESULT} EQUAL 0) 

message(STATUS "current Git revision is: ${MCTDHF_GIT_REVISION}")

endif(NOT EXISTS ${CMAKE_SOURCE_DIR}/src/versions.hpp)

endmacro(GetGitRevision)

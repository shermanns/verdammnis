/*  ***  carlotta_gkba_params.h  ***  */

/*  ***  carlotta gkba get params  ***  */
void carlotta_gkba_get_params(struct t_params* params)
{
    /*  ***  get parameters  ***  */

    char data[strlength];
    FILE* pptr;
    /*  ***  display  ***  */
    printf("| parameters '%s'\n", option_initfile);
    pptr = fopen(option_initfile, "r");
    if (pptr == NULL)
    {
        printf("| carlotta_dm_mpi_get_params(): ERROR [failed to open '%s']\n",
               option_initfile);
        printf("|_exit_to_system!______________________________________________"
               "\n\n");
        abort();
    }
    else
    {
        while (feof(pptr) == 0)
        {
            if (fgets(data, strlength, pptr) != NULL)
            {
                if (data[0] == ' ')
                    continue;
            }
            if (strstr(data, "<int:ns>"))
            {
                if (feof(pptr) == 0)
                {
                    if (fgets(data, strlength, pptr) != NULL)
                    {
                        params->ns = atoi(data);
                    }
                    else
                    {
                        printf(
                            "Error reading from config file: ns, Exiting.\n");
                        exit(0);
                    }
                }
            }
            if (strstr(data, "<int:dim>"))
            {
                if (feof(pptr) == 0)
                {
                    if (fgets(data, strlength, pptr) != NULL)
                    {
                        params->dim = atoi(data);
                    }
                    else
                    {
                        printf(
                            "Error reading from config file: dim, Exiting.\n");
                        exit(0);
                    }
                }
            }
            if (strstr(data, "<int:size>"))
            {
                if (feof(pptr) == 0)
                {
                    if (fgets(data, strlength, pptr) != NULL)
                    {
                        params->size = atoi(data);
                    }
                    else
                    {
                        printf("Error reading from config file: sq_ns, "
                               "Exiting.\n");
                        exit(0);
                    }
                }
            }
            if (strstr(data, "<int:ringmode>"))
            {
                if (feof(pptr) == 0)
                {
                    if (fgets(data, strlength, pptr) != NULL)
                    {
                        params->ringmode = atoi(data);
                    }
                    else
                    {
                        printf("Error reading from config file: ringmode, "
                               "Exiting.\n");
                        exit(0);
                    }
                }
            }
            if (strstr(data, "<int:mode>"))
            {
                if (feof(pptr) == 0)
                {
                    if (fgets(data, strlength, pptr) != NULL)
                    {
                        params->mode = atoi(data);
                    }
                    else
                    {
                        printf(
                            "Error reading from config file: mode, Exiting.\n");
                        exit(0);
                    }
                }
            }
            if (strstr(data, "<int:as>"))
            {
                if (feof(pptr) == 0)
                {
                    if (fgets(data, strlength, pptr) != NULL)
                    {
                        params->as = atoi(data);
                    }
                    else
                    {
                        printf(
                            "Error reading from config file: as, Exiting.\n");
                        exit(0);
                    }
                }
            }
            if (strstr(data, "<int:propt>"))
            {
                if (feof(pptr) == 0)
                {
                    if (fgets(data, strlength, pptr) != NULL)
                    {
                        params->propt = atoi(data);
                    }
                    else
                    {
                        printf("Error reading from config file: prop_t, "
                               "Exiting.\n");
                        exit(0);
                    }
                }
            }
            if (strstr(data, "<int:npart>"))
            {
                if (feof(pptr) == 0)
                {
                    if (fgets(data, strlength, pptr) != NULL)
                    {
                        params->npart = atoi(data);
                    }
                    else
                    {
                        printf("Error reading from config file: n_part, "
                               "Exiting.\n");
                        exit(0);
                    }
                }
            }
            if (strstr(data, "<int:spin>"))
            {
                if (feof(pptr) == 0)
                {
                    if (fgets(data, strlength, pptr) != NULL)
                    {
                        params->spin = atoi(data);
                    }
                    else
                    {
                        printf(
                            "Error reading from config file: spin, Exiting.\n");
                        exit(0);
                    }
                }
            }
            if (strstr(data, "<real:beta>"))
            {
                if (feof(pptr) == 0)
                {
                    if (fgets(data, strlength, pptr) != NULL)
                    {
                        params->beta = atof(data);
                    }
                    else
                    {
                        printf(
                            "Error reading from config file: beta, Exiting.\n");
                        exit(0);
                    }
                }
            }
            if (strstr(data, "<real:dt>"))
            {
                if (feof(pptr) == 0)
                {
                    if (fgets(data, strlength, pptr) != NULL)
                    {
                        params->dt = atof(data);
                    }
                    else
                    {
                        printf(
                            "Error reading from config file: dt, Exiting.\n");
                        exit(0);
                    }
                }
            }
            if (strstr(data, "<real:ad_tau>"))
            {
                if (feof(pptr) == 0)
                {
                    if (fgets(data, strlength, pptr) != NULL)
                    {
                        params->ad_tau = atof(data);
                    }
                    else
                    {
                        printf("Error reading from config file: ad_tau, "
                               "Exiting.\n");
                        exit(0);
                    }
                }
            }
            if (strstr(data, "<real:ad_t0>"))
            {
                if (feof(pptr) == 0)
                {
                    if (fgets(data, strlength, pptr) != NULL)
                    {
                        params->ad_t0 = atof(data);
                    }
                    else
                    {
                        printf("Error reading from config file: ad_t0, "
                               "Exiting.\n");
                        exit(0);
                    }
                }
            }
            if (strstr(data, "<real:t1>"))
            {
                if (feof(pptr) == 0)
                {
                    if (fgets(data, strlength, pptr) != NULL)
                    {
                        params->t1 = atof(data);
                    }
                    else
                    {
                        printf("Error reading from config file: "
                               "t1, Exiting.\n");
                        exit(0);
                    }
                }
            }
            if (strstr(data, "<real:t2>"))
            {
                if (feof(pptr) == 0)
                {
                    if (fgets(data, strlength, pptr) != NULL)
                    {
                        params->t2 = atof(data);
                    }
                    else
                    {
                        printf("Error reading from config file: "
                               "t2, Exiting.\n");
                        exit(0);
                    }
                }
            }
            if (strstr(data, "<real:lambda>"))
            {
                if (feof(pptr) == 0)
                {
                    if (fgets(data, strlength, pptr) != NULL)
                    {
                        params->lambda = atof(data);
                    }
                    else
                    {
                        printf("Error reading from config file: lambda, "
                               "Exiting.\n");
                        exit(0);
                    }
                }
            }
            if (strstr(data, "<int:ex_mode>"))
            {
                if (feof(pptr) == 0)
                {
                    if (fgets(data, strlength, pptr) != NULL)
                    {
                        params->ex_mode = atoi(data);
                    }
                    else
                    {
                        printf("Error reading from config file: ex_mode, "
                               "Exiting.\n");
                        exit(0);
                    }
                }
            }
            if (strstr(data, "<real:t0>"))
            {
                if (feof(pptr) == 0)
                {
                    if (fgets(data, strlength, pptr) != NULL)
                    {
                        params->t0 = atof(data);
                    }
                    else
                    {
                        printf(
                            "Error reading from config file: t0, Exiting.\n");
                        exit(0);
                    }
                }
            }
            if (strstr(data, "<real:w0>"))
            {
                if (feof(pptr) == 0)
                {
                    if (fgets(data, strlength, pptr) != NULL)
                    {
                        params->w0 = atof(data);
                    }
                    else
                    {
                        printf(
                            "Error reading from config file: w0, Exiting.\n");
                        exit(0);
                    }
                }
            }
            if (strstr(data, "<int:calc_colint>"))
            {
                if (feof(pptr) == 0)
                {
                    if (fgets(data, strlength, pptr) != NULL)
                    {

                        params->calc_colint = atoi(data);
                    }
                    else
                    {
                        printf("Error reading from config file: calc_colint, "
                               "Exiting.\n");
                        exit(0);
                    }
                }
            }
            if (strstr(data, "<int:frs>"))
            {
                if (feof(pptr) == 0)
                {
                    if (fgets(data, strlength, pptr) != NULL)

                    {
                        params->frs = atoi(data);
                    }
                    else
                    {
                        printf(
                            "Error reading from config file: frs, Exiting.\n");
                        exit(0);
                    }
                }
            }
        }
    }
    fclose(pptr);
}

/*  ***  carlotta gkba params  ***  */
void carlotta_gkba_params(struct t_params* params)
{

    /*  ***  carlotta dm mpi get params  ***  */
    carlotta_gkba_get_params(params);

    /*  ***  derived params  ***  */
    params->t = ((real)(params->propt - 1)) * params->dt;
    params->spin_factor = params->spin + 1.0;

    /*  ***  display  ***  */

    printf("| npart=%d spin=%d beta=%f\n", params->npart, params->spin,
           params->beta);
    printf("| ns=%d, dim=%d, size=%d\n", params->ns, params->dim, params->size);
    printf("| lambda=%f\n", params->lambda);
    printf("| prop_t=%d  dt=%f  t=%f \n", params->propt, params->dt, params->t);
    if (params->ex_mode == 3)
    {
        printf("| ex_mode=%d  t0=%f w0=%f frs=%d\n", params->ex_mode,
               params->t0, params->w0, params->frs);
    }
    else
    {
        printf("| ex_mode=%d  t0=%f w0=%f\n", params->ex_mode, params->t0,
               params->w0);
    }
}

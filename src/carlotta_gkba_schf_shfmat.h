/*  ***  carlotta_gkba_schf_shfmat.h  ***  */

/*  ***  carlotta gkba schf shfmat  ***  */
void carlotta_gkba_schf_shfmat(struct t_params* params, struct t_mat* mat,
                               struct t_schf* schf, double* dmat)
{
    int i, j;
    const int ns = params->ns;
    /*  ***  construct (Hartree-Fock energy) shfmat[]  ***  */
    for (i = 0; i < ns; i++)
    {
        for (j = 0; j < ns; j++)
        {
            schf->shfmat[i + ns * j] = dmat[i + ns * j] * mat->wmat[i + ns * j];
        }
    }
}

#ifndef CARLOTTA_GKBA_MAT_WMAT_H
#define CARLOTTA_GKBA_MAT_WMAT_H

void carlotta_gkba_construct_wmat(struct t_params* params, struct t_mat* mat,
                                  real lambda)
{
    int i, j;
    const int ns = params->ns;

    /*  ***  construct (interaction energy) wmat[]  ***  */
    for (i = 0; i < ns; ++i)
    {
        for (j = 0; j < ns; ++j)
        {
            if (i == j)
            {
                mat->wmat[i + ns * j] = lambda;
            }
            else
            {
                mat->wmat[i + ns * j] = 0;
            }
        }
    }
}
#endif // *** CARLOTTA_GKBA_MAT_WMAT_H *** //

/*
 * carlotta_gkba_prop_observables.h
 *
 *  Created on: 18.01.2011
 *      Author: sh
 */

#ifndef CARLOTTA_GKBA_PROP_OBSERVABLES_H_
#define CARLOTTA_GKBA_PROP_OBSERVABLES_H_
#include <complex.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <math.h>
#include <time.h>

void carlotta_gkba_prop_observables(struct t_params* params,
                                    struct t_schf* schf, struct t_mat* mat,
                                    int t, complex double* colint,
                                    double sswitch, realc* gless_et)
{
    const int ns = params->ns;
    char dfile[strlength];
    char dfile1[strlength];
    char dfile2[strlength];
    char filename[100];
    char filename1[100];
    char filename2[100];
    FILE* dptr, *dptr1, *dptr2;
    int i, j, k;
    complex double sumat, sumpot, hf_docc;
    complex double sumhfe;
    complex double sumce;
    complex double sumte;
    complex double sumparticle;
    complex double* hf_docc_i = rcvector(ns);
    int ad_t0_steps;

    /* *** Check particle number conservation *** */
    if (1 == 1)
    {
        sumparticle = 0;
        for (i = 0; i < ns; i++)
        {
            sumparticle += -I * gless_et[i + ns * i];
        }
        printf("Particle number: %.15e    ", cabs(sumparticle));
    }
    // *** Calculate Energies *** //
    if (1 == 1)
    {
        sumhfe = 0;
        sumce = 0;
        if (params->mode != 0 && params->calc_colint == 1)
        {
            for (i = 0; i < ns; ++i)
            {
                sumce += params->spin_factor * cimag(0.5 * colint[i]);
            }
        }
        sumat = 0;
        hf_docc = 0;
        sumpot = 0;

        for (i = 0; i < ns; i++)
        {
            for (k = 0; k < ns; ++k)
            {
                sumat += params->spin_factor * mat->hmat[i + ns * k] * -I *
                         gless_et[k + ns * i];
                hf_docc +=
                    schf->shf_prop[i + ns * k] * -I * gless_et[k + ns * i];
            }
            sumhfe += hf_docc;
            hf_docc_i[i] = hf_docc;
            hf_docc = 0.0;

            // *** compute E_corr ***
        }
        if ((params->ex_mode == 2) && (t >= (params->t0 / params->dt)))
        {
            sumpot += params->spin_factor * params->w0 * -I * gless_et[0];
        }
        sumte = (sumat + sumhfe + sumce + sumpot);

        if (1 == 1)
        {
            printf("E_at: %.15e    ", creal(sumat));
        }

        if (0 == 1)
        {
            printf("E_pot: %.15e    ", creal(sumpot));
        }

        if (1 == 1)
        {
            printf("E_HF: %.15e    ", creal(sumhfe));
        }

        if (1 == 1)
        {
            printf("E_corr: %.15e    ", creal(sumce));
        }

        if (1 == 1)
        {
            printf("E_tot: %.15e    \n", creal(sumte));
        }
    }

    if (t % 1 == 0) // t % ...
    {
        //  ***  write "prop.dat"  *** //
        sprintf(filename, "results/prop_ns%d_n%d_l%.2f_w%.2f.dat", params->ns,
                params->npart, params->lambda, params->w0);
        strcpy(dfile, filename);
        if (0 == 0)
        {
            if (t == 0)
            {
                dptr = fopen(dfile, "w");
                fprintf(dptr, "# [t] [switch] [particle number] [atomic "
                              "energy] [potential energy] [hf energy] [corr "
                              "energy] [total energy]\n");
                fclose(dptr);
            }
            dptr = fopen(dfile, "a");
            fprintf(dptr, "%.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e\n",
                    t * params->dt, creal(sswitch), creal(sumparticle),
                    creal(sumat), creal(sumpot), creal(sumhfe), creal(sumce),
                    creal(sumte));
            fclose(dptr);
        }
        ad_t0_steps = (int)2 * params->ad_t0 / params->dt;
        //  ***  write "prop_density.dat"  *** //
        sprintf(filename, "results/density_ns%d_n%d_l%.2f_w%.2f.dat",
                params->ns, params->npart, params->lambda, params->w0);
        strcpy(dfile, filename);
        sprintf(filename1, "results/odensity_ns%d_n%d_l%.2f_w%.2f.dat",
                params->ns, params->npart, params->lambda, params->w0);
        strcpy(dfile1, filename1);
        sprintf(filename2, "results/doub_occ_ns%d_n%d_l%.2f_w%.2f.dat",
                params->ns, params->npart, params->lambda, params->w0);
        strcpy(dfile2, filename2);

        if (params->as == 1)
        {
            if (t > ad_t0_steps)
            {
                if (t * params->dt == 2 * params->ad_t0 + params->dt)
                {
                    dptr = fopen(dfile, "w");
                    dptr1 = fopen(dfile1, "w");
                    dptr2 = fopen(dfile2, "w");
                    fprintf(dptr, "# [t] [n(i)]\n");
                    fprintf(dptr1, "# [t] [n(i+ns*j)]\n");
                    fprintf(dptr2,
                            "# [t] [d_occ_HF(i)] [d_occ_corr(i)] [d_occ(i)]\n");
                    fclose(dptr);
                    fclose(dptr1);
                    fclose(dptr2);
                }

                dptr = fopen(dfile, "a");
                dptr1 = fopen(dfile1, "a");
                dptr2 = fopen(dfile2, "a");
                fprintf(dptr, "%.15e ", t * params->dt);
                fprintf(dptr1, "%.15e ", t * params->dt);
                fprintf(dptr2, "%.15e ", t * params->dt);

                for (i = 0; i < ns; ++i)
                {
                    for (j = 0; j < ns; ++j)
                    {
                        if (i == j)
                        {
                            fprintf(dptr, "%.15e ",
                                    creal(-I * gless_et[i + ns * j]));
                            fprintf(dptr2, "%.15e %.15e %.15e ",
                                    1.0 / params->lambda * creal(hf_docc_i[i]),
                                    1.0 / params->lambda * params->spin_factor *
                                        cimag(0.5 * colint[i]),
                                    1.0 / params->lambda * creal(hf_docc_i[i]) +
                                        1.0 / params->lambda *
                                            params->spin_factor *
                                            cimag(0.5 * colint[i]));
                        }
                        else
                        {
                            fprintf(dptr1, "%.15e ",
                                    creal(-I * gless_et[i + ns * j]));
                        }
                    }
                }
                fprintf(dptr, "\n");
                fprintf(dptr1, "\n");
                fprintf(dptr2, "\n");
                fclose(dptr);
                fclose(dptr1);
                fclose(dptr2);
            }
        }
        else
        {
            if (t == 0)
            {
                dptr = fopen(dfile, "w");
                dptr1 = fopen(dfile1, "w");
                dptr2 = fopen(dfile2, "w");
                fprintf(dptr, "# [t] [n(i)]\n");
                fprintf(dptr1, "# [t] [n(i+ns*j)]\n");
                fprintf(dptr2,
                        "# [t] [d_occ_HF(i)] [d_occ_corr(i)] [d_occ(i)]\n");
                fclose(dptr);
                fclose(dptr1);
                fclose(dptr2);
            }

            dptr = fopen(dfile, "a");
            dptr1 = fopen(dfile1, "a");
            dptr2 = fopen(dfile2, "a");
            fprintf(dptr, "%.15e ", t * params->dt);
            fprintf(dptr1, "%.15e ", t * params->dt);
            fprintf(dptr2, "%.15e ", t * params->dt);

            for (i = 0; i < ns; ++i)
            {
                for (j = 0; j < ns; ++j)
                {
                    if (i == j)
                    {
                        fprintf(dptr, "%.15e ",
                                creal(-I * gless_et[i + ns * j]));
                        fprintf(dptr2, "%.15e %.15e %.15e ",
                                1.0 / params->lambda * creal(hf_docc_i[i]),
                                1.0 / params->lambda * params->spin_factor *
                                    cimag(0.5 * colint[i]),
                                1.0 / params->lambda * creal(hf_docc_i[i]) +
                                    1.0 / params->lambda * params->spin_factor *
                                        cimag(0.5 * colint[i]));
                    }
                    else
                    {
                        fprintf(dptr1, "%.15e ",
                                creal(-I * gless_et[i + ns * j]));
                    }
                }
            }
            fprintf(dptr, "\n");
            fprintf(dptr1, "\n");
            fprintf(dptr2, "\n");
            fclose(dptr);
            fclose(dptr1);
            fclose(dptr2);
        }
    }
    free_rcvector(hf_docc_i, ns);
}
#endif /* CARLOTTA_GKBA_PROP_OBSERVABLES_H_ */

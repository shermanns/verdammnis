/*  ***  carlotta_gkba_msg.h  ***  */

/*  ***  carlotta dm mpi stamp, time and date message  ***  */
char *msgtd(char msg[])
  {
  time_t actual_time;
  struct tm *timeinfo;
  /*  ***  actual time  ***  */
  time(&actual_time);
  /*  ***  decompose into local time elements  ***  */
  timeinfo=localtime(&actual_time);
  /*  ***  generate stamp  ***  */
  strftime(msg, 255, "stamp  carlotta gkba  ver. 1.0  %c", timeinfo);
  return msg;
  }

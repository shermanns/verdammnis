/*
 * carlotta_gkba.c
 *
 *  Created on: 11.10.2010
 *      Author: sh
 */

/*  ***  global precompiler definitions  ***  */
#define PI 3.14159265358979323846264338327950288419716939937511
#define strlength 255
#define precision 'd'  // 's': single precision, 'd': double precision
/*  ***  set precision  ***  */
#if precision=='d'
#define real double
#define realc complex double

#define I cdouble(0.0,1.0)
#define lapacksygvd_ dsygvd_
#define lapackheev_ zheev_
#else
#define real float
#define realc float complex
#define I cfloat(0.0,1.0)
#define lapacksygvd_ ssygvd_
#define lapackheev_ cheev_
#endif
//#include <mkl.h>
/*  ***  global includes  ***  */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <math.h>
#include <complex.h>
#include <time.h>

/*  ***  global option variables  ***  */
int option_c = 0; // correlations off (0), on (1)
int option_f = 0; // formatted std output of observables off (0), on (1)
int option_gkba = 0; // mode of generalized kadanoff-baym ansatz
int option_p = 0; // only display parameters (0)
int option_t = 0; // time measure off (0), on (1)
int option_v = 0; // 3rd collision integrals not added (0), added (1)
char option_initfile[strlength]; // initfile name
real option_memory = 1.0e9; // default memory 1 GB geändert

/*  ***  carlotta gkba integr constants  ***  */
const float cg10[5] =
{ 0.329861111111111, 1.320833333333333, 0.766666666666666, 1.101388888888888, 0.981250000000000 };
const float c5[5] =
{ 14.0 / 45.0, 64.0 / 45.0, 24.0 / 45.0, 64.0 / 45.0, 14.0 / 45.0 };
const float c4[4] =
{ 3.0 / 8.0, 9.0 / 8.0, 9.0 / 8.0, 3.0 / 8.0 };
const float c3[3] =
{ 1.0 / 3.0, 4.0 / 3.0, 1.0 / 3.0 };

/*  ***  headers  ***  */
#include "carlotta_gkba_msg.h"
#include "carlotta_gkba_alloc.h"
#include "carlotta_gkba_typedef.h"
#include "carlotta_gkba_params.h"
#include "carlotta_gkba_lapack.h"
#include "carlotta_gkba_mat_hmat.h"
#include "carlotta_gkba_mat_wmat.h"
#include "carlotta_gkba_mat.h"
#include "carlotta_gkba_schf_display.h"
#include "carlotta_gkba_schf_observables.h"
#include "carlotta_gkba_schf_shfmat.h"
#include "carlotta_gkba_schf_mu.h"
#include "carlotta_gkba_schf.h"
#include "carlotta_gkba_prop_observables.h"
#include "carlotta_gkba_prop.h"

/*  ***  main get options  ***  */
void main_get_options(int argc, char *argv[])
{
	int c;
	while (1)
	{
		static struct option long_options[] =
		{
		{ "correlations", no_argument, 0, 'c' },
		{ "formatted", no_argument, 0, 'f' },
		{ "gkba", required_argument, 0, 'g' },
		{ "parameters", no_argument, 0, 'p' },
		{ "initfile", required_argument, 0, 'i' },
		{ "memory", required_argument, 0, 'm' },
		{ "time", no_argument, 0, 't' },
		{ "3rd collision integrals", no_argument, 0, 'v' },
		{ 0, 0, 0, 0 } };
		/*  ***  option index  ***  */
		int option_index = 0;
		c = getopt_long(argc, argv, "cfg:pi:m:tv", long_options, &option_index);
		/*  ***  detect end of options  ***  */
		if (c == -1)
			break;
		/*  ***  options  ***  */
		switch (c)
		{
		case 'c':
			printf("| use option -c (%s [on])\n", long_options[0].name);
			option_c = 1;
			break;
		case 'f':
			printf("| use option -f (%s [std output])\n", long_options[1].name);
			option_f = 1;
			break;
		case 'g':
			printf("| use option -g %s (%s [mode])\n", optarg, long_options[2].name);
			option_gkba = atoi(optarg);
			break;
		case 'p':
			printf("| use option -p (%s [display only])\n", long_options[3].name);
			option_p = 1;
			break;
		case 'i':
			printf("| use option -i %s (%s [*.ini])\n", optarg, long_options[4].name);
			strcpy(option_initfile, optarg);
			break;
		case 'm':
			printf("| use option -m %s (%s [in bytes])\n", optarg, long_options[5].name);
			option_memory = atof(optarg);
			break;
		case 't':
			printf("| use option -t (%s [measure on])\n", long_options[6].name);
			option_t = 1;
			break;
		case 'v':
			printf("| use option -v (%s [added])\n", long_options[7].name);
			option_v = 1;
			break;
		case '?': /*  ***  getopt_long already printed an error message  ***  */
			break;
		default:
			abort();
			break;
		}
	}
	printf("| ---\n");
}

/*  ***  carlotta gkba header  ***  */
void carlotta_gkba_header(int argc, char *argv[])
{
	char msg[strlength];

	/*  ***  display  ***  */
	printf(" ______________________________________________________________\n");
	printf("| C A R L O T T A _ G K B A . P R G\n");
	printf("| \"carlotta_gkba.prg\"  copyright (c) september 2010  cau kiel\n");
	printf("| %s\n", msgtd(msg));
	printf("| by  dipl. phys. karsten balzer\n");
	printf("| email  balzer@theo-physik.uni-kiel.de\n");
	printf("| location  ls15-r341\n");
	printf("| phone  +49 (0)431 880 4070\n");
	printf("| ---\n");
	if (precision == 'd')
		printf("| cmpld  'double precision' (%c, real=%d B, realc=%d B)\n", precision, (int) sizeof(real), (int) sizeof(realc));
	else
		printf("| cmpld  'single precision' (%c, real=%d B, realc=%d B)\n", precision, (int) sizeof(real), (int) sizeof(realc));

	/*  ***  main get options  ***  */
	main_get_options(argc, argv);
}

/*  ***  carlotta gkba footer  ***  */
void carlotta_gkba_footer()
{

	/*  ***  display  ***  */
	printf("| total cpu time \n");
	printf("|_all_done!____________________________________________________\n\n");

}

/*  ***  main  ***  */
int main(int argc, char *argv[])
{

	/*  ***  type definition  ***  */
	struct t_params *params;
	struct t_mat *mat;
	struct t_schf *schf;

	/*  ***  allocate memory  ***  */
	params = (struct t_params*) malloc(sizeof(struct t_params));
	mat = (struct t_mat*) malloc(sizeof(struct t_mat));
	schf = (struct t_schf*) malloc(sizeof(struct t_schf));

	/*  ***  carlotta gkba header  ***  */
	carlotta_gkba_header(argc, argv);

	/*  ***  carlotta gkba params  ***  */
	carlotta_gkba_params(params);

	/*  ***  carlotta gkba mat def  ***  */
	carlotta_gkba_mat_def(params, mat);

	/*  ***  carlotta gkba mat  ***  */
	carlotta_gkba_mat(params, mat);

	/*  ***  carlotta gkba schf def  ***  */
	carlotta_gkba_schf_def(params, schf);

	/*  ***  carlotta gkba schf  ***  */
	carlotta_gkba_schf(params, mat, schf);

	/* *** carlotta gkba prop def *** */
	carlotta_gkba_prop_def(params, schf);

	/* *** carlotta gkba prop *** */
	carlotta_gkba_prop(params, schf, mat);

	/* *** carlotta gkba prop undef *** */
	carlotta_gkba_prop_undef(params, schf);

	/*  ***  carlotta gkba schf undef  ***  */
	carlotta_gkba_schf_undef(params, schf);

	/*  ***  carlotta gkba mat undef  ***  */
	carlotta_gkba_mat_undef(params, mat);

	/*  ***  carlotta gkba footer  ***  */
	carlotta_gkba_footer();

	/*  ***  free memory  ***  */
	free(params);
	free(mat);
	free(schf);

	/*  ***  return  ***  */
	return 0;
}

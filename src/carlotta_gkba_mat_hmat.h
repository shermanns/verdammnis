#ifndef CARLOTTA_GKBA_MAT_HMAT_H
#define CARLOTTA_GKBA_MAT_HMAT_H

//  ***  carlotta_gkba_mat_hmat.h  ***  //

/*  ***  construct gkba (hopping energy) hmat[]  ***  */
void carlotta_gkba_construct_hmat(struct t_params* params, struct t_mat* mat)
{
    int i, j, plane;
    const int ns = params->ns;
    const int size = params->size;
    const int size_sq = size * size;
    double t1 = params->t1;
    double t2 = params->t2;

    if (params->dim == 1)
    {
        for (i = 0; i < ns; ++i)
        {
            for (j = 0; j < ns; ++j)
            {
                mat->hmat[i + ns * j] = 0.0;
                if ((i == j - 1) || (i == j + 1) ||
                    (params->ringmode == 1 &&
                     ((i == 0 && j == ns - 1) || (i == ns - 1 && j == 0))))
                {
                    mat->hmat[i + ns * j] = t1;
                }
                if ((i == j - 2) || (i == j + 2) ||
                    (params->ringmode == 1 &&
                     ((i == 0 && j == ns - 2) || (i == ns - 2 && j == 0) ||
                      (i == 1 && j == ns - 1) || (i == ns - 1 && j == 1))))
                {
                    mat->hmat[i + ns * j] = t2;
                }
            }
        }
        if (params->ringmode == 1)
        {
            mat->hmat[ns - 1 + ns * 0] = t1;
            mat->hmat[0 + ns * (ns - 1)] = t1;
        }
    }
    if (params->dim == 2)
    {
        for (i = 0; i < ns; ++i)
        {
            for (j = 0; j < ns; ++j)
            {
                if ((((i == j + 1) || (i == j - 1)) &&
                     (i / size == j / size)) ||
                    (i % size == j % size && (i - j == size || j - i == size)))
                {
                    mat->hmat[i + ns * j] = t1;
                    // mat->hmat[i + ns * j] = 0.0;
                }
                else
                {
                    mat->hmat[i + ns * j] = 0;
                }
            }
        }
        if (params->ringmode == 1)
        {
            for (i = 0; i < ns; ++i)
            {
                if (i % size == 0) /// left-hand side
                {
                    mat->hmat[i + ns * (i + size - 1)] = t1;
                }
                if (i % size == size - 1) /// right-hand side
                {
                    mat->hmat[i + ns * (i - size + 1)] = t1;
                }
                if (i / size == 0) /// top side
                {
                    mat->hmat[i + ns * (i + size * (size - 1))] = t1;
                }
                if (i / size == size - 1) /// bottom side
                {
                    mat->hmat[i + ns * (i - size * (size - 1))] = t1;
                }
            }
        }
    }
    if (params->dim == 3)
    {
        for (plane = 0; plane < size; ++plane) /// setup 2d-planes
        {
            for (i = plane * size_sq; i < plane * size_sq + size_sq; ++i)
            {
                for (j = plane * size_sq; j < plane * size_sq + size_sq; ++j)
                {
                    if ((((i == j + 1) || (i == j - 1)) &&
                         (i / size == j / size)) ||
                        (i % size == j % size &&
                         (i - j == size || j - i == size)))
                    {
                        mat->hmat[i + ns * j] = t1;
                        // mat->hmat[i + ns * j] = 0.0;
                    }
                    else
                    {
                        mat->hmat[i + ns * j] = 0;
                    }
                }
            }
            /// Connect 2d(x-y)-planes in z-direction
            if (plane == 0) /// front
            {
                for (i = 0; i < size_sq; ++i)
                {
                    mat->hmat[i + ns * (i + size_sq)] = t1;
                }
            }
            else
            {
                if (plane == size - 1) /// back
                {
                    for (i = (size - 1) * size_sq; i < size * size_sq; ++i)
                    {
                        mat->hmat[i + ns * (i - size_sq)] = t1;
                    }
                }
                else
                {
                    for (i = plane * size_sq; i < (plane + 1) * size_sq; ++i)
                    {
                        mat->hmat[i + ns * (i + size_sq)] = t1;
                        mat->hmat[i + ns * (i - size_sq)] = t1;
                    }
                }
            }
            if (params->ringmode == 1)
            {
                /// PBC for 2d planes
                for (i = plane * size_sq; i < plane * size_sq + size_sq; ++i)
                {
                    if (i % size == 0) /// left-hand side
                    {
                        mat->hmat[i + ns * (i + size - 1)] = t1;
                    }
                    if (i % size == size - 1) /// right-hand side
                    {
                        mat->hmat[i + ns * (i - size + 1)] = t1;
                    }
                    if (i / size == 0) /// top side
                    {
                        mat->hmat[i + ns * (i + size * (size - 1))] = t1;
                    }
                    if (i / size == size - 1) /// bottom side
                    {
                        mat->hmat[i + ns * (i - size * (size - 1))] = t1;
                    }
                }
                /// PBC for front and back in 3d
                if (plane == 0) /// front
                {
                    for (i = 0; i < size_sq; ++i)
                    {
                        mat->hmat[i + ns * (i + size_sq * (size - 1))] = t1;
                        mat->hmat[(i + size_sq * (size - 1)) + ns * i] = t1;
                    }
                }
            }
        }
    }
}

void setHmatForAdiabaticSwitching(struct t_params* params, struct t_mat* mat)
{
    int i = 0;
    int j = 0;
    int ns = params->ns;
    if (params->ex_mode == 3)
    {
        for (i = params->frs; i < ns; ++i)
        {
            mat->hmat[i + ns * i] = params->w0;
        }
    }
    if (params->ex_mode == 4)
    {
        // Doublons initially set in the middle of the chain
        int half = ns / 2; /// really integer division, since we start counting
                           /// from zero
        int show = 1;
        printf("\n %d Doublon(s) set to the middle, i.e., sites ",
               params->npart);
        if (ns % 2 == 1) // ns uneven
        {
            if (params->npart % 2 == 1) // number of doublons uneven
            {
                for (i = 0; i < ns; ++i)
                {
                    mat->hmat[i + ns * i] = params->w0;
                    if ((half - i <= (params->npart) / 2 && half >= i) ||
                        (i - half - 1 < ((params->npart - 1) / 2) && i >= half))
                    {
                        if (show == 1)
                        {
                            printf("%d ", i + 1);
                            show = 0;
                        }
                        mat->hmat[i + ns * i] = 0.0;
                    }
                    else
                    {
                        if (show == 0)
                        {
                            printf("to %d (counting from 1 to ns)\n", i);
                            show = 1;
                        }
                    }
                }
            }
            else // number of doublons even
            {
                for (i = 0; i < ns; ++i)
                {
                    mat->hmat[i + ns * i] = params->w0;
                    if ((half - i < (params->npart) / 2 && half >= i) ||
                        (i - half - 1 <= ((params->npart - 1) / 2) &&
                         i >= half))
                    {
                        if (show == 1)
                        {
                            printf("%d ", i + 1);
                            show = 0;
                        }
                        mat->hmat[i + ns * i] = 0.0;
                    }
                    else
                    {
                        if (show == 0)
                        {
                            printf("to %d (counting from 1 to ns)\n", i);
                            show = 1;
                        }
                    }
                }
            }
        }
        else // ns even
        {
            if (params->npart % 2 == 1) // number of doublons uneven
            {
                for (i = 0; i < ns; ++i)
                {
                    mat->hmat[i + ns * i] = params->w0;
                    if (abs(i - half) <= (params->npart - 1) / 2)
                    {
                        if (show == 1)
                        {
                            printf("%d ", i + 1);
                            show = 0;
                        }
                        mat->hmat[i + ns * i] = 0.0;
                    }
                    else
                    {
                        if (show == 0)
                        {
                            printf("to %d (counting from 1 to ns)\n", i);
                            show = 1;
                        }
                    }
                }
            }
            else // number of doublons even
            {
                for (i = 0; i < ns; ++i)
                {
                    mat->hmat[i + ns * i] = params->w0;
                    if ((half - i <= (params->npart) / 2 && half >= i) ||
                        (i - half - 1 < ((params->npart - 1) / 2) && i >= half))
                    {
                        if (show == 1)
                        {
                            printf("%d ", i + 1);
                            show = 0;
                        }
                        // printf("i: %d \n",i);
                        mat->hmat[i + ns * i] = 0.0;
                    }
                    else
                    {
                        if (show == 0)
                        {
                            printf("to %d (counting from 1 to ns)\n", i);
                            show = 1;
                        }
                    }
                }
            }
        }
    }
}
#endif // CARLOTTA_GKBA_MAT_HMAT_H //

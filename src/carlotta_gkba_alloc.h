/*  ***  carlotta_gkba_alloc.h  ***  */

#define NR_END 1
#define FREE_ARG char*
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <math.h>
#include <complex.h>
#include <time.h>




/*  ***  integer vector  ***  */
int *ivector(int mdim) {
  /*  ***  update mpi_memory  ***  */

   return (int*)malloc(mdim*sizeof(int));
  }
/*  ***  free integer vector  ***  */
void free_ivector(int *iv, int mdim) {
  free(iv);

  }

/*  ***  real vector  ***  */
real *rvector(int mdim) {


   return (real*)malloc(mdim*sizeof(real));
  }
/*  ***  free real vector  ***  */
void free_rvector(real *rv, int mdim) {
  free(rv);

  }

/*  ***  double vector  ***  */
double *dvector(int mdim) {

  return (double*)malloc(mdim*sizeof(double));
  }
/*  ***  free double vector  ***  */
void free_dvector(double *rv, int mdim) {
  free(rv);

  }

/*  ***  complex real vector  ***  */
realc *rcvector(int mdim) {

  return (realc*)malloc(mdim*sizeof(realc));
  }
/*  ***  free complex real vector  ***  */
void free_rcvector(realc *rcv, int mdim) {
  free(rcv);

  }

/*  ***  real matrix  ***  */
real **rmatrix(int mdim_row, int mdim_col) {

  int i;
  real **m;
  /*  ***  not contiguous  ****  */
  //m=(real**)malloc(mdim_row*sizeof(real));
  //for(i=0;i<mdim_row;i++) m[i]=(real*)malloc(mdim_col*sizeof(real));
  /*  ***  contiguous  ****  */
  m=(real**)malloc(mdim_row*sizeof(real));
  m[0]=(real*)malloc(mdim_row*mdim_col*sizeof(real));
  for(i=0;i<mdim_row;i++) m[i]=m[0]+i*mdim_col;
  return m;
  }
/*  ***  free real matrix  ***  */
void free_rmatrix(real **m, int mdim_row, int mdim_col)
  {
  /*  ***  not contiguous  ****  */
  //int i;
  //for(i=0;i<mdim_row;i++) free(m[i]);
  //free(m);
  /*  ***  contiguous  ****  */
  free(m);

  }

/*  ***  double matrix  ***  */
double **dmatrix(int mdim_row, int mdim_col) {
   int i;
  double **m;
  /*  ***  not contiguous  ****  */
  //m=(double**)malloc(mdim_row*sizeof(double));
  //for(i=0;i<mdim_row;i++) m[i]=(double*)malloc(mdim_col*sizeof(double));
  /*  ***  contiguous  ****  */
  m=(double**)malloc(mdim_row*sizeof(double));
  m[0]=(double*)malloc(mdim_row*mdim_col*sizeof(double));
  for(i=0;i<mdim_row;i++) m[i]=m[0]+i*mdim_col;
  return m;
  }
/*  ***  free double matrix  ***  */
void free_dmatrix(double **m, int mdim_row, int mdim_col)
  {
  /*  ***  not contiguous  ****  */
  //int i;
  //for(i=0;i<mdim_row;i++) free(m[i]);
  //free(m);
  /*  ***  contiguous  ****  */
  free(m);
    }

/*  ***  real complex matrix  ***  */
realc **rcmatrix(int mdim_row, int mdim_col) {
  /*  ***  update mpi_memory  ***  */
   int i;
  realc **m;
  /*  ***  not contiguous  ****  */
  //m=(realc**)malloc(mdim_row*sizeof(realc));
  //for(i=0;i<mdim_row;i++) m[i]=(realc*)malloc(mdim_col*sizeof(realc));
  /*  ***  contiguous old  ****  */
  //m=(realc**)malloc(mdim_row*sizeof(realc));
  //realc *temp=(realc*)malloc(mdim_row*mdim_col*sizeof(realc));
  //for(i=0;i<mdim_row;i++) {
  //  m[i]=temp+(i*mdim_col);
  //  }
  /*  ***  contiguous new (example sebastian)  ****  */
  //cdouble **mpipsi;
  //mpipsi=new cdouble*[Nr];
  //mpipsi[0]=new cdouble[Nx*Nr];
  //for (int i=0; i<Nr; i++) mpipsi[i]=mpipsi[0]+i*Nx;
  /*  ***  contiguous new  ****  */
  m=(realc**)malloc(mdim_row*sizeof(realc));
  m[0]=(realc*)malloc(mdim_row*mdim_col*sizeof(realc));
  for(i=0;i<mdim_row;i++) m[i]=m[0]+i*mdim_col;
  return m;
  }
/*  ***  free real complex matrix  ***  */
void free_rcmatrix(realc **m, int mdim_row, int mdim_col)
  {
  /*  ***  not contiguous  ****  */
  //int i;
  //for(i=0;i<mdim_row;i++) free(m[i]);
  //free(m);
  /*  ***  contiguous  ****  */
  free(m);
   }


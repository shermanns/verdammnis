/*  ***  carlotta_gkba_schf_mu.h  ***  */

/*  ***  carlotta gkba schf mu  ***  */
#define NPART_ACCURACY 1.0e-10
real carlotta_gkba_schf_mu(int ns, int npart, real beta, real *cval)
  {
  int i, k;
  int cn=0, ci=0;
  real mu, dmu;
  real mu1, mu2;
  real npart_mu;
  real sign=1.0;

  /*  ***  detect step #1  ***  */
  cn=0;
  for(i=0;i<ns;i++) {
    mu=creal(cval[i]);
    npart_mu=0.0;
    for(k=0;k<=ns-1;k++) if ((beta*(cval[k]-mu))<700.0) npart_mu+=1.0/(exp(beta*(cval[k]-mu))+sign);
    if ((npart_mu>((real)npart))&&(cn==0)) { ci=i-1; cn=1; }
    }

  /*  ***  detect step #2 (search from below)  ***  */
  dmu=cval[ci+1]-cval[ci]/10.0;
  mu=cval[ci]+dmu;
  do {
    npart_mu=0.0;
    for(k=0;k<=ns-1;k++) if ((beta*(cval[k]-mu))<700.0) npart_mu+=1.0/(exp(beta*(cval[k]-mu))+sign);
    if ((dmu<0.0)&&((npart_mu-(real)npart)<0.0)) dmu=-dmu/2.0;
    if ((dmu>0.0)&&((npart_mu-(real)npart)>0.0)) dmu=-dmu/2.0;
    if (fabs(npart_mu-((real)npart))>NPART_ACCURACY) mu+=dmu;
    }
    while (fabs(npart_mu-((real)npart))>NPART_ACCURACY);
  mu1=mu;

  /*  ***  detect step #3 (search from above)  ***  */
  dmu=cval[ci+1]-cval[ci]/10.0;
  mu=cval[ci+1]-dmu;
  do {
    npart_mu=0.0;
    for(k=0;k<=ns-1;k++) if ((beta*(cval[k]-mu))<700.0) npart_mu+=1.0/(exp(beta*(cval[k]-mu))+sign);
    if ((dmu<0.0)&&((npart_mu-(real)npart)>0.0)) dmu=-dmu/2.0;
    if ((dmu>0.0)&&((npart_mu-(real)npart)<0.0)) dmu=-dmu/2.0;
    if (fabs(npart_mu-((real)npart))>NPART_ACCURACY) mu-=dmu;
    }
    while (fabs(npart_mu-((real)npart))>NPART_ACCURACY);
  mu2=mu;

  /*  ***  display  ***  */
  //printf("|   | chemical potential:\n");
  //printf("|   |   | mu1: %.10f (%.10f)\n", mu1, npart_mu1);
  //printf("|   |   | mu2: %.10f (%.10f)\n", mu2, npart_mu2);
  //printf("|   |   | mu: %.10f (%.10f)\n", 0.5*(mu1+mu2), 0.5*(npart_mu1+npart_mu2));

  /*  ***  return  ***  */
  return 0.5*(mu1+mu2);
  }
#undef NPART_ACCURACY

/*  ***  carlotta gkba schf distrib  ***  */
inline real carlotta_gkba_schf_distrib(real beta, real cval, real mu)
  {
  real d=beta*(cval-mu);

  if (d>700.0) return 0.0;
  else return 1.0/(exp(d)+1.0);
  }


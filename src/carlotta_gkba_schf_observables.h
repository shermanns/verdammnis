/*  ***  carlotta_gkba_observables.h  ***  */

/*  ***  carlotta gkba observables  ***  */
void carlotta_gkba_schf_observables(struct t_params *params, struct t_mat *mat, struct t_schf *schf)
{
	int i, j;
	const int ns = params->ns;
	const int ns_sq = params->ns * params->ns;

	/*  ***  compute energies  ***  */
	real energy_hop = 0.0;
	real energy_hf = 0.0;
	real energy_kin = 0.0;
	real energy_pot = 0.0;
	for (i = 0; i < ns; i++)
		for (j = 0; j < ns; j++)
		{
			const real su = schf->dmat1[j + ns * i];
			energy_hop += params->spin_factor * mat->hmat[i + ns * j] * su;
			energy_kin += params->spin_factor * mat->tmat[i + ns * j] * su;
			energy_pot += params->spin_factor * (mat->hmat[i + ns * j]-mat->tmat[i + ns * j]) * su;
			if ((params->npart == 1) && (params->spin == 0))
			{
			}
			else
				energy_hf += 0.5 * params->spin_factor * schf->shfmat[i + ns * j] * su;
		}
	schf->energy_hop = energy_kin;
	schf->energy_hf = energy_hf;
	schf->energy_tot = schf->energy_hop + schf->energy_hf;

	/* ***  density matrix checks  *** */
	real trrho = 0.0;
	for (i = 0; i < ns; i++)
	{
		for (j = 0; j < ns; j++)
		{
			if (i == j)
			{
				trrho += schf->dmat1[j + ns * i];
			}
		}
	}
	schf->trrho = trrho;

	/*  ***  compute acc  ***  */
	real acc = 0.0;
	for (i = 0; i < ns; i++)
		for (j = 0; j < ns; j++)
		{
			acc += fabs(fabs(schf->dmat0[i + ns * j]) - fabs(schf->dmat1[i + ns * j]));
		}
	schf->acc = acc / ((real)(ns_sq));

}

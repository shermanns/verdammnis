#ifndef CARLOTTA_GKBA_MAT_LAPACK_H
#define CARLOTTA_GKBA_MAT_LAPACK_H

/*  ***  extern  ***  */
extern void lapackheev_(char* JOBZ, char* UPLO, int* mdim, realc* A, int* LDA,
                        real* W, realc* WORK, int* LWORK, real* RWORK,
                        int* INFO);
extern void lapacksygvd_(int* ITYPE, char* JOBZ, char* UPLO, int* N, double* A,
                         int* LDA, double* B, int* LDB, double* W, double* WORK,
                         int* LWORK, int* IWORK, int* LIWORK, int* INFO);
extern void zgemm_(char* TRANSA, char* TRANSB, int* M, int* N, int* K,
                   realc* ALPHA, realc* A, int* LDA, realc* B, int* LDB,
                   realc* BETA, realc* C, int* LDC);

/*  ***  converts a matrix to a vector  ***  */
inline real* convertmat_rvec1(real** mat, int ze, int sp)
{
    int i, j;
    real* retval = rvector(ze * sp);
    for (i = 0; i < ze; i++)
        for (j = 0; j < sp; j++)
            retval[i + ze * j] = mat[i][j];
    return retval;
}

/*  ***  converts a complex matrix to a vector  ***  */
inline realc* convertmat_cvec1(realc** mat, int ze, int sp)
{
    int i, j;
    realc* retval = rcvector(ze * sp);
    for (i = 0; i < ze; i++)
        for (j = 0; j < sp; j++)
            retval[i + ze * j] = mat[i][j];
    return retval;
}

/*  ***  converts a vector to a matrix  ***  */
inline realc** convertcvec1_mat(realc* vec, int ze, int sp)
{
    int i, j;
    realc** retval = rcmatrix(ze, sp);
    for (i = 0; i < ze; i++)
        for (j = 0; j < sp; j++)
            retval[i][j] = vec[i + ze * j];
    return retval;
}

/*  ***  converts a matrix to a vector  ***  */
inline double* convertmat_dvec1(double** mat, int ze, int sp)
{
    int i, j;
    double* retval = dvector(ze * sp);
    for (i = 0; i < ze; i++)
        for (j = 0; j < sp; j++)
            retval[i + ze * j] = mat[i][j];
    return retval;
}

/*  ***  carlotta gkba lapack ddiag  ***  */
void carlotta_gkba_lapack_ddiag(int mdim, double** mat1, double** mat2,
                                double* mval, double** mvec)
{
    int i, j;
    double* vec1 = convertmat_dvec1(mat1, mdim, mdim);
    double* vec2 = convertmat_dvec1(mat2, mdim, mdim);
    int INFO;
    int ITYPE = 1;
    char UPLO = 'L';
    char JOBZ = 'V';
    int LWORK = 1 + 6 * mdim + 2 * mdim * mdim;
    int LIWORK = 3 + 5 * mdim;
    int* IWORK = ivector(LIWORK);
    double* WORK = dvector(LWORK);

    /*  ***  LAPACK ROUTINE  ***  */
    lapacksygvd_(&ITYPE, &JOBZ, &UPLO, &mdim, vec1, &mdim, vec2, &mdim, mval,
                 WORK, &LWORK, IWORK, &LIWORK, &INFO);
    if (INFO == 0)
    {
        for (i = 0; i < mdim; i++)
            for (j = 0; j < mdim; j++)
                mvec[i][j] = vec1[i * mdim + j];
    }
    else
    {
        if (INFO < 0)
            printf("| lapack: the %d-th argument had an illegal value\n", INFO);
        else
            printf("| lapack: failure ... \n");
    }

    /*  ***  free memory  ***  */
    free_dvector(WORK, LWORK);
    free_ivector(IWORK, LIWORK);
    free_dvector(vec1, mdim * mdim);
    free_dvector(vec2, mdim * mdim);
}

/*  ***  carlotta gkba lapack vecmatmulti new  ***  */
void vecmatmulti(int mdim, realc alpha, char transa, realc* A, char transb,
                 realc* B, realc beta, realc* C)
{

    zgemm_(&transa, &transb, &mdim, &mdim, &mdim, &alpha, A, &mdim, B, &mdim,
           &beta, C, &mdim);
}

/*  ***  carlotta gkba lapack rcdiag new  ***  */
void carlotta_gkba_lapack_rcdiag(int mdim, realc* mat, real* mval, realc* mvec)
{
    // int i, j;
    int LDA = mdim, LWORK = 2 * mdim - 1, INFO;
    char JOBZ = 'V';
    char UPLO = 'L';
    // real *W;
    real* RWORK;
    realc* A;
    realc* WORK;

    /*  ***  allocate memory  ***  */
    A = rcvector(mdim * mdim);
    // W=rvector(mdim);
    WORK = rcvector(LWORK);
    RWORK = rvector(3 * mdim - 2);

    /*  ***  set up matrix A old  ***  */
    // for(i=0;i<mdim;i++)
    //  for(j=0;j<mdim;j++) A[i+mdim*j]=mat[i+mdim*j];
    /*  ***  set up matrix A new  ***  */
    memcpy(A, mat, mdim * mdim * sizeof(realc));

    /*  ***  LAPACK ROUTINE old  ***  */
    // lapackheev_(&JOBZ, &UPLO, &mdim, A, &LDA, W, WORK, &LWORK, RWORK, &INFO);
    /*  ***  LAPACK ROUTINE new  ***  */
    lapackheev_(&JOBZ, &UPLO, &mdim, A, &LDA, mval, WORK, &LWORK, RWORK, &INFO);
    if (INFO == 0)
    {
        // printf("| lapack: successful exit.\n");
    }
    else
    {
        if (INFO < 0)
            printf("| lapack: the %d-th argument had an illegal value.\n",
                   -INFO);
        if (INFO > 0)
            printf("| lapack: the algorithm failed to converge.\n");
    }

    /*  ***  save eigenvalues in mval[] and eigenvectors in mvec[][] old  ***
     */
    // for(i=0;i<mdim;i++) {
    //  mval[i]=W[i];
    //  for(j=0;j<mdim;j++) mvec[i+mdim*j]=A[i+mdim*j];
    //  }
    /*  ***  save eigenvalues in mval[] and eigenvectors in mvec[][] new  ***
     */
    memcpy(mvec, A, mdim * mdim * sizeof(realc));

    /*  ***  free memory  ***  */
    free_rcvector(A, mdim * mdim);
    // free_rvector(W, mdim);
    free_rcvector(WORK, LWORK);
    free_rvector(RWORK, 3 * mdim - 2);
}

#endif // CARLOTTA_GKBA_LAPACK_H //

/*  ***  carlotta_gkba_typedef.h  ***  */

/*  ***  struct t_params  ***  */
struct t_params
{
    int ns;
    int size;
    int dim;
    int ringmode;
    int mode;
    int as;
    int propt; // number of time steps
    int npart;
    int spin;
    double spin_factor;
    double dt;
    double ad_tau;
    double ad_t0;
    double t;
    double lambda;
    double t1;
    double t2;   // nnn hopping amplitude
    int ex_mode; // field parameter
    int frs;
    double w0;   // field parameter
    double t0;   // field parameter
    double beta;
    int calc_colint;
};

/*  ***  struct t_mat  ***  */
struct t_mat
{
    real* hmat;
    real* wmat;
    real* tmat;
    real* vmat;
};

/*  ***  struct t_schf  ***  */
struct t_schf
{
    real mu;
    real spe;
    real hfe;
    real ce;
    real te;
    real energy_tot;
    real energy_hop;
    real energy_hf;
    real trrho; // Trace of rho
    real acc;   // accuracy of iteration procedure
    real* cval; // HF eigenvalues (i=0...ns-1)
    real* dmat0;
    real* dmat1; // HF density matrix in fedrv representation
    // real *sigma0;
    realc* sigmahf; // HF self-energy
    real* shfmat;   // HF self-energy
    realc* cvec0;
    realc* cvec1; // HF eigenvectors (m=0...ns*ns-1) [i+ns*j]
    realc* hmat;  // T+V+Sigma
    real** wmat;
    realc** gless_et;             // g< [t][i+ns*j]
    realc* uvec;                  // U(Delta) [i +ns*j]
    realc** xmat_prop;            // X[i + ns * j] [t] for all t < T
    realc* shf_prop;              // mat of Sigma_HF[i + ns * j][T] for actual T
    realc* h_prop;                // H(t)
    realc* gless_prop;            // g<(tbar, t)
    realc* ggreater_prop;         // g> (tbar, t)
    realc** sigmaless_prop_tbart; // sigma<(tbar,t)
    realc** sigmagreater_prop_ttbar; // sigma>(t,tbar)
    realc** sigmaless_prop_ttbar;    // sigma<(t,tbar)
    realc** sigmagreater_prop_tbart; // sigma>(tbar,t)
};

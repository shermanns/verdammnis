/*  ***  carlotta_gkba_mat.h  ***  */

/*  ***  carlotta gkba mat def  ***  */
void carlotta_gkba_mat_def(struct t_params *params, struct t_mat *mat)
{
	int i, j;
	const int ns = params->ns;
	const int ns_sq = params->ns * params->ns;

	/*  ***  allocate memory  ***  */
	mat->hmat = rvector(ns_sq);
	mat->wmat = rvector(ns_sq);
	mat->tmat = rvector(ns_sq);
	mat->vmat = rvector(ns_sq);

	/*  ***  initialize matrices  ***  */
	for (i = 0; i < ns; i++)
	{
		for (j = 0; j < ns; j++)
		{
			mat->hmat[i + ns * j] = 0.0;
			mat->wmat[i + ns * j] = 0.0;
			mat->tmat[i + ns * j] = 0.0;
			mat->vmat[i + ns * j] = 0.0;
		}
	}
}

/*  ***  carlotta gkba mat undef  ***  */
void carlotta_gkba_mat_undef(struct t_params *params, struct t_mat *mat)
{
	const int ns_sq = params->ns * params->ns;

	/*  ***  free memory  ***  */
	free_rvector(mat->wmat, ns_sq);
	free_rvector(mat->hmat, ns_sq);
	free_rvector(mat->tmat, ns_sq);
	free_rvector(mat->vmat, ns_sq);
}

/*  ***  carlotta gkba mat  ***  */
void carlotta_gkba_mat(struct t_params *params, struct t_mat *mat)
{
	/*  ***  set matrices  ***  */

	/*  ***  construct (hopping energy) hmat[]  ***  */
	//printf("| construct hmat[]\n");
	carlotta_gkba_construct_hmat(params, mat);
	/*  ***  construct (interaction energy) wmat[]  ***  */
	//printf("| construct wmat[]\n");
	if (params->as == 0) // AS off
	{
		carlotta_gkba_construct_wmat(params, mat, params->lambda);
	}
	else // AS on
	{
		carlotta_gkba_construct_wmat(params, mat, 0.0);
	}
}


/*
 * carlotta_gkba_prop.h
 *
 *  Created on: 11.01.2011
 *      Author: sh
 */

#ifndef CARLOTTA_GKBA_PROP_H
#define CARLOTTA_GKBA_PROP_H

//#define DEBUG
#include <complex.h>
#include "carlotta_gkba_mat_hmat.h"
#include "carlotta_gkba_mat_wmat.h"
#include "carlotta_gkba_lapack.h"
/*  ***  carlotta gkba schf shfmat  ***  */
void carlotta_gkba_schf_shfprop(struct t_params* params, struct t_mat* mat,
                                struct t_schf* schf, realc* gless)
{
    int i, j;
    const int ns = params->ns;

    /*  ***  construct (Hartree-Fock energy) shfmat[]  ***  */
    for (i = 0; i < ns; i++)
    {
        for (j = 0; j < ns; j++)
        {
            schf->shf_prop[j + ns * i] =
                -I * gless[j + ns * i] * mat->wmat[j + ns * i];
        }
    }
}
double getSwitch(struct t_params* Parameter, int TimeStep)
{
    return (Parameter->lambda *
            (1.0 -
             (1.0 / (1.0 + exp(((TimeStep * Parameter->dt - Parameter->ad_t0)) /
                               Parameter->ad_tau)))));
}

void calcXmat(struct t_params* params, struct t_schf* schf, realc* onens, int t)
{
    int tbar = 0;
    int ns = params->ns;
    realc* mat1 = rcvector(ns * ns);
    for (tbar = 0; tbar <= t; ++tbar)
    {

        if (t == 0) // für t=0 nicht benötigt, da xmat bekannt

        {
            memcpy(schf->xmat_prop[tbar], onens,
                   ns * ns * sizeof(complex double));
        }
        else
        {
            if (t == tbar)
            {
                memcpy(schf->xmat_prop[tbar], onens,
                       ns * ns * sizeof(complex double));
            }
            else
            {
                vecmatmulti(ns, 1.0, 'N', schf->xmat_prop[tbar], 'C',
                            schf->uvec, 0.0, mat1);
                memcpy(schf->xmat_prop[tbar], mat1,
                       ns * ns * sizeof(complex double));
            }
        }
    }
}

struct timeval tv;
double mtime(void)
{
    gettimeofday(&tv, NULL);
    return (double)(tv.tv_sec + (tv.tv_usec / 1000000.0));
}

void carlotta_gkba_prop_def(struct t_params* params, struct t_schf* schf)
{
    const int ns_sq = params->ns * params->ns;
    /*  ***  allocate memory  ***  */

    if (params->mode == 1) // GKBA
    {
        schf->xmat_prop = rcmatrix(params->propt + 2, ns_sq);
        schf->gless_et = rcmatrix(params->propt + 2, ns_sq); // +2 wegen g<[T+1]
    }
    // *************************************************************************************
    // for HF, GKBA

    schf->shf_prop = rcvector(ns_sq);
    schf->h_prop = rcvector(ns_sq);
    schf->uvec = rcvector(ns_sq);
}

void carlotta_gkba_prop_undef(struct t_params* params, struct t_schf* schf)
{
    const int ns_sq = params->ns * params->ns;
    /*  ***  free memory  ***  */

    if (params->mode == 1) // GKBA
    {
        free_rcmatrix(schf->xmat_prop, params->propt + 2, ns_sq);
    }

    // for HF, GKBA
    free_rcmatrix(schf->gless_et, params->propt + 2, ns_sq);
    free_rcvector(schf->shf_prop, ns_sq);
    free_rcvector(schf->h_prop, ns_sq);
    free_rcvector(schf->uvec, ns_sq);
}

void carlotta_gkba_prop(struct t_params* params, struct t_schf* schf,
                        struct t_mat* mat)
{
    const int ns = params->ns;
    int i, j, k, l, m, n, t, tbar;
    double sswitch;
    char filename[100];
    char filename1[100];
    complex double* sumc = rcvector(ns * ns);
    complex double* vec0 = rcvector(ns * ns);
    double* val = rvector(ns);
    complex double* mat1 = rcvector(ns * ns);
    complex double* mat2 = rcvector(ns * ns);
    complex double* mat3 = rcvector(ns * ns);
    complex double* onens = rcvector(ns * ns);
    complex double* zerons = rcvector(ns * ns);
    complex double* gless = rcvector(ns * ns);
    realc* scolint = rcvector(ns);
    char msg[strlength];
    FILE* dptr, *dptr1;
    double density;
    int ad_t0_steps;
    char dfile[strlength];
    char dfile1[strlength];

    // *** Initialization START *** //
    // **************************************************************************************

    for (j = 0; j < ns; j++)
    {
        for (i = 0; i < ns; i++)
        {
            if (params->mode == 1) /// GKBA
            {
                schf->gless_et[0][j + ns * i] =
                    I * schf->dmat1[j + ns * i]; // Initialize g^<
            }
            else /// HF
            {
                gless[j + ns * i] = I * schf->dmat1[j + ns * i];
            }

            sumc[j + ns * i] = 0;
            zerons[j + ns * i] = 0;
            if (i == j)
            {
                onens[j + ns * i] = 1;
            }
            else
            {
                onens[j + ns * i] = 0;
            }
        }
    }
    memcpy(scolint, zerons, ns * sizeof(realc));

    // *** Initialization END *** //
    // ****************************************************************************************

    // *** Time propagation START *** //
    // ************************************************************************************
    double time0 = mtime();
    double time1 = 0;
    double time2 = 0;
    double time3 = 0;
    double time4 = 0;

    setHmatForAdiabaticSwitching(params, mat);

    for (t = 0; t <= params->propt; ++t)
    {
        time1 = mtime();

        /* *** Compute SigmaHF for current t *** */

        /*  ***  construct (Hartree-Fock energy) shfmat[]  ***  */
        if (params->as == 1)
        {
            // *** use adiabatic switched wmat *** //
            sswitch = getSwitch(params, t);
            carlotta_gkba_construct_wmat(params, mat, sswitch);
            if (params->mode == 1) /// GKBA
            {
                carlotta_gkba_schf_shfprop(params, mat, schf,
                                           schf->gless_et[t]);
            }
            else
            {
                carlotta_gkba_schf_shfprop(params, mat, schf, gless);
            }
        }
        else
        {
            if (params->mode == 1) /// GKBA
            {
                carlotta_gkba_schf_shfprop(params, mat, schf,
                                           schf->gless_et[t]);
            }
            else
            {
                carlotta_gkba_schf_shfprop(params, mat, schf, gless);
            }
        }

        /* *** Calculate h_prop for t *** */

        for (j = 0; j < ns; j++)
        {
            for (i = 0; i < ns; i++)
            {
                schf->h_prop[j + ns * i] =
                    mat->hmat[j + ns * i] + schf->shf_prop[j + ns * i];
            }
        }
        if ((params->ex_mode == 1) &&
            ((t == (int)(params->t0 / params->dt)) ||
             (t == (int)(params->t0 / params->dt) + 1)))
        {
            schf->h_prop[0] = params->w0 + schf->shf_prop[0];
        }
        if ((params->ex_mode == 2) && (t >= (params->t0 / params->dt)))
        {
            schf->h_prop[0] = params->w0 + schf->shf_prop[0];
        }
        if ((params->ex_mode == 3) && (t == (int)(params->t0 / params->dt)))
        {
            carlotta_gkba_construct_hmat(params, mat);
        }
        if ((params->ex_mode == 4) && (t == (int)(params->t0 / params->dt)))
        {
            carlotta_gkba_construct_hmat(params, mat);
        }
        /* *** Compute X(tmax+Delta,tmax) = U(Delta) *** */

        carlotta_gkba_lapack_rcdiag(ns, schf->h_prop, val, vec0);

        for (j = 0; j < ns; j++)
        {
            for (i = 0; i < ns; i++)
            {
                if (i == j)
                {
                    mat1[j + ns * i] = cexp(-I * params->dt * val[i]);
                }
                else
                {
                    mat1[j + ns * i] = 0;
                }
            }
        }
        vecmatmulti(ns, 1.0, 'N', mat1, 'C', vec0, 0.0, mat2);
        vecmatmulti(ns, 1.0, 'N', vec0, 'N', mat2, 0.0, schf->uvec);

        if (params->mode == 1) // GKBA

        {
            calcXmat(params, schf, onens, t);
            time2 = mtime();

#ifdef DEBUG
            for (tbar = 0; tbar <= t; ++tbar)
            {
                for (i = 0; i < ns; ++i)
                {
                    for (j = 0; j < ns; ++j)
                    {
                        if (t == 1)
                        {

                            printf("tbar: %d, i: %d, j: %d, Imagglesstbar: "
                                   "%.5e, Realglesstbar %.5e\n",
                                   tbar, i, j,
                                   cimag(schf->gless_et[tbar][i + ns * j]),
                                   creal(schf->gless_et[tbar][i + ns * j]));
                        }
                    }
                }
            }
            for (tbar = 0; tbar <= t; ++tbar)
            {
                for (i = 0; i < ns; ++i)
                {
                    for (j = 0; j < ns; ++j)
                    {
                        if (t == 1)
                        {

                            printf("tbar: %d, i: %d, j: %d, Imagxmattbar: "
                                   "%.5e, Realxmattbar %.5e\n",
                                   tbar, i, j,
                                   cimag(schf->xmat_prop[tbar][i + ns * j]),
                                   creal(schf->xmat_prop[tbar][i + ns * j]));
                        }
                    }
                }
            }
#endif

            //#pragma omp parallel
            //			{
            //#pragma omp for private(i,j)
            realc* gglij = rcvector(ns * ns);
            memcpy(gglij, zerons, ns * ns * sizeof(realc));
            memcpy(scolint, zerons, ns * sizeof(realc));
            for (tbar = 0; tbar <= t; ++tbar)
            {
                if (t > 0) // für t=0 nicht benötigt, da xmat bekannt

                {

                    for (i = 0; i < ns; i++)
                    {
                        for (j = i; j < ns; j++)
                        {
                            if (j == i && params->calc_colint == 1)
                            {
                                realc ggik;
                                realc glki;
                                for (k = 0; k < ns; k++)
                                {
                                    ggik = 0;
                                    glki = 0;
                                    for (l = 0; l < ns; l++)
                                    {
                                        glki +=
                                            schf->gless_et[tbar][k + ns * l] *
                                            schf->xmat_prop[tbar][l + ns * i];
                                        if (l == k)
                                        {
                                            ggik +=
                                                conj(schf->xmat_prop
                                                         [tbar][l + ns * i]) *
                                                (schf->gless_et[tbar]
                                                               [l + ns * k] -
                                                 I);
                                        }
                                        else
                                        {
                                            ggik +=
                                                conj(schf->xmat_prop
                                                         [tbar][l + ns * i]) *
                                                schf->gless_et[tbar]
                                                              [l + ns * k];
                                        }
                                    }
                                    if ((tbar == 0) || (tbar == t))
                                    {
                                        scolint[i] += getSwitch(params, tbar) *
                                                      ggik * ggik * glki * glki;
                                    }
                                    else
                                    {
                                        scolint[i] += 2 *
                                                      getSwitch(params, tbar) *
                                                      ggik * ggik * glki * glki;
                                    }
                                }
                            }
                            else
                            {
                                realc ggik;
                                realc ggjk;
                                realc glki;
                                realc glkj;
                                for (k = 0; k < ns; k++)
                                {
                                    ggik = 0;
                                    ggjk = 0;
                                    glki = 0;
                                    glkj = 0;
                                    for (l = 0; l < ns; l++)
                                    {
                                        glki +=
                                            schf->gless_et[tbar][k + ns * l] *
                                            schf->xmat_prop[tbar][l + ns * i];
                                        glkj +=
                                            schf->gless_et[tbar][k + ns * l] *
                                            schf->xmat_prop[tbar][l + ns * j];
                                        if (l == k)
                                        {
                                            ggik +=
                                                conj(schf->xmat_prop
                                                         [tbar][l + ns * i]) *
                                                (schf->gless_et[tbar]
                                                               [l + ns * k] -
                                                 I);
                                            ggjk +=
                                                conj(schf->xmat_prop
                                                         [tbar][l + ns * j]) *
                                                (schf->gless_et[tbar]
                                                               [l + ns * k] -
                                                 I);
                                        }
                                        else
                                        {
                                            ggik +=
                                                conj(schf->xmat_prop
                                                         [tbar][l + ns * i]) *
                                                schf->gless_et[tbar]
                                                              [l + ns * k];
                                            ggjk +=
                                                conj(schf->xmat_prop
                                                         [tbar][l + ns * j]) *
                                                schf->gless_et[tbar]
                                                              [l + ns * k];
                                        }
                                    }
#ifdef DEBUG
                                    printf("tbar: %d, i: %d, j: %d, Imaggless: "
                                           "%.5e, Realgless %.5e\n",
                                           tbar, k, i, cimag(glki),
                                           creal(glki));
                                    printf(
                                        "tbar: %d, i: %d, j: %d, Imaggreater: "
                                        "%.5e, Realggreater %.5e\n",
                                        tbar, i, k, cimag(ggik), creal(ggik));
#endif
                                    if ((tbar == 0) || (tbar == t))
                                    {
                                        gglij[i + ns * j] +=
                                            0.5 * getSwitch(params, tbar) *
                                            (ggik * glkj *
                                                 (-ggik * glki + ggjk * glkj) +
                                             conj(ggjk * glki * (-ggjk * glkj +
                                                                 glki * ggik)));
                                        gglij[j + ns * i] +=
                                            0.5 * getSwitch(params, tbar) *
                                            (ggjk * glki *
                                                 (-ggjk * glkj + ggik * glki) +
                                             conj(ggik * glkj * (-ggik * glki +
                                                                 glkj * ggjk)));
                                    }
                                    else
                                    {
                                        gglij[i + ns * j] +=
                                            getSwitch(params, tbar) *
                                            (ggik * glkj *
                                                 (-ggik * glki + ggjk * glkj) +
                                             conj(ggjk * glki * (-ggjk * glkj +
                                                                 glki * ggik)));
                                        gglij[j + ns * i] +=
                                            getSwitch(params, tbar) *
                                            (ggjk * glki *
                                                 (-ggjk * glkj + ggik * glki) +
                                             conj(ggik * glkj * (-ggik * glki +
                                                                 glkj * ggjk)));
                                    }
                                }
                            }
#ifdef DEBUG
                            printf("tbar: %d, i: %d, j: %d, Imagcolinttbar: "
                                   "%.5e, Realcolinttbar %.5e\n",
                                   tbar, i, j, cimag(gglij[i + ns * j]),
                                   creal(gglij[i + ns * j]));
                            if (i == j)
                            {
                                printf(
                                    "tbar: %d, i: %d, j: %d, Imagscolinttbar: "
                                    "%.5e, Realscolinttbar %.5e\n",
                                    tbar, i, j, cimag(precolint[i]),
                                    creal(precolint[i]));
                            }
#endif
                        }
                    }

                } // Ende if t>0

            } // Ende von tbar for
            //	} Ende omp parallel
            time3 = mtime();
            // *** Reduce colint *** //
            for (i = 0; i < ns; ++i)
            {
                scolint[i] *= getSwitch(params, t) * params->dt;
            }
            for (i = 0; i < ns; ++i)
            {
                for (j = 0; j < ns; ++j)
                {
                    sumc[i + ns * j] = I * params->dt * params->dt *
                                       getSwitch(params, t) * gglij[i + ns * j];
#ifdef DEBUG
                    printf("i: %d, j: %d, Imagsumc: %.5e, Realsumc %.5e\n", i,
                           j, cimag(sumc[i + ns * j]), creal(sumc[i + ns * j]));
#endif
                    sumc[i + ns * j] += schf->gless_et[t][i + ns * j];
                }
            }
        } // Ende von GKBA
          // ***********************************************************************************************************************************************************************************//

        // *** Compute G^<(t+Delta,t+Delta) *** //
        if (params->mode == 0) // HF
        {
            vecmatmulti(ns, 1.0, 'N', gless, 'N', onens, 0.0, sumc);
        }

        vecmatmulti(ns, 1.0, 'N', sumc, 'C', schf->uvec, 0, mat1); // +sumc
                                                                   // gless
        if (params->mode == 1)                                     /// GKBA
        {
            vecmatmulti(ns, 1.0, 'N', schf->uvec, 'N', mat1, 0,
                        schf->gless_et[t + 1]); // gless
        }

        // *** Write observables to screen / file *** //
        printf("\n t: %d\n", t);
        if (params->mode == 1) /// GKBA
        {
            carlotta_gkba_prop_observables(params, schf, mat, t, scolint,
                                           getSwitch(params, t),
                                           schf->gless_et[t]);
        }
        else
        {
            carlotta_gkba_prop_observables(params, schf, mat, t, scolint,
                                           getSwitch(params, t), gless);
        }
        if (params->mode == 0) /// HF
        {
            vecmatmulti(ns, 1.0, 'N', schf->uvec, 'N', mat1, 0,
                        gless); /// write gless[t+1] into gless
        }
        time4 = mtime();
        if (0 == 0)
        {
            if (t == 0)
            {
                dptr = fopen("results/timer.dat", "w");
                fprintf(dptr, "# %s\n", msgtd(msg));
                fprintf(dptr, "# [t] [elapsed time] [used time for nonparallel "
                              "this timestep] [parallel]\n");
                fclose(dptr);
            }
            dptr = fopen("results/timer.dat", "a");
            fprintf(dptr, "%.15e %.15e %.15e %.15e\n", t * params->dt,
                    time1 - time0, time2 - time1 + time4 - time3,
                    time3 - time2);
            fclose(dptr);
        }
    }
    /*  ***  free local memory  ***  */
    free_rvector(val, ns);
    free_rcvector(vec0, ns * ns);
    free_rcvector(sumc, ns * ns);
    free_rcvector(mat1, ns * ns);
    free_rcvector(mat2, ns * ns);
    free_rcvector(mat3, ns * ns);
    free_rcvector(onens, ns * ns);
    free_rcvector(zerons, ns * ns);
    free_rcvector(gless, ns * ns);
    free_rcvector(scolint, ns * ns);
}
#endif // *** CARLOTTA_GKBA_PROP_H *** //

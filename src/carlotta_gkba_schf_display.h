/*
 * carlotta_gkba_schf_display.h
 *
 *  Created on: 01.11.2010
 *      Author: sh
 */

#ifndef CARLOTTA_GKBA_SCHF_DISPLAY_H_
#define CARLOTTA_GKBA_SCHF_DISPLAY_H_

/*  ***  carlotta gkba hfgf def  ***  */
void carlotta_gkba_schf_display_header()
{
	printf("| ---\n");
	if (option_f == 0)
	{
		char msg[strlength];
		printf("# %s\n", msgtd(msg));
		printf("# [nit] [acc]                 [mu]                  [energy_tot]          [energy_hop]          [energy_hf]           [npart]\n");
	}
}
void carlotta_gkba_schf_display(struct t_schf *schf, int nit)
{
	if (option_f == 0)
	{
		printf("  %d     %.15e %.15e %.15e %.15e %.15e %.15e\n", nit, schf->acc, schf->mu, schf->energy_tot, schf->energy_hop,
				schf->energy_hf, schf->trrho);
	}
	else
	{
		printf("| nit=%d  acc=%.3e  mu=%.6f  energy_tot=%.6f (hop: %.6f, hf: %.6f)\n", nit, schf->acc, schf->mu, schf->energy_tot,
				schf->energy_hop, schf->energy_hf);
	}
}

void carlotta_gkba_schf_display_dmat(struct t_schf *schf, struct t_params *params)
{
	const int ns = params->ns;
	int i, j;
	for (j = 0; j < ns; j++)
	{
		const int nsj = ns * j;
		for (i = 0; i < ns; i++)
		{
			{
				printf("%.5e ", schf->dmat0[i + nsj]);
			}
		}
		printf("\n");
	}
	printf("\n");
}

#endif /* CARLOTTA_GKBA_SCHF_DISPLAY_H_ */

/*  ***  carlotta_gkba_schf.h  ***  */

/*  ***  carlotta gkba schf def  ***  */
void carlotta_gkba_schf_def(struct t_params* params, struct t_schf* schf)
{
    const int ns = params->ns;
    const int ns_sq = params->ns * params->ns;
    /*  ***  allocate memory  ***  */
    schf->cval = rvector(ns);
    schf->dmat0 = rvector(ns_sq);
    schf->dmat1 = rvector(ns_sq);
    schf->shfmat = rvector(ns_sq);
    schf->cvec0 = rcvector(ns_sq);
    schf->cvec1 = rcvector(ns_sq);
    schf->hmat = rcvector(ns_sq);
}

/*  ***  carlotta gkba schf undef  ***  */
void carlotta_gkba_schf_undef(struct t_params* params, struct t_schf* schf)
{
    const int ns = params->ns;
    const int ns_sq = params->ns * params->ns;

    /*  ***  free memory  ***  */
    free_rvector(schf->cval, ns);
    free_rvector(schf->dmat0, ns_sq);
    free_rvector(schf->dmat1, ns_sq);
    free_rvector(schf->shfmat, ns_sq);
    free_rcvector(schf->cvec0, ns_sq);
    free_rcvector(schf->cvec1, ns_sq);
    free_rcvector(schf->hmat, ns_sq);
}

/*  ***  carlotta gkba schf  ***  */
void carlotta_gkba_schf(struct t_params* params, struct t_mat* mat,
                        struct t_schf* schf)
{
    int i, j, k;
    int nit = 0;
    const int ns = params->ns;
    const int ns_sq = params->ns * params->ns;
    const int npart = params->npart;
    const real alpha = 0;
    const real beta = params->beta;
    const real acc_req = 1.0e-15;

    carlotta_gkba_schf_display_header(schf, nit);

    /*  ***  init dmat1[] and cvec1[]  ***  */
    for (j = 0; j < ns; j++)
    {
        const int nsj = ns * j;
        for (i = 0; i < ns; i++)
        {
            if (i == j)
                schf->dmat1[i + nsj] = 0.77;
            else
                schf->dmat1[i + nsj] = -0.65;
            schf->cvec1[i + nsj] = 0.0;
        }
    }

    /*  ***  iterate  ***  */
    do
    {

        /*  ***  set dmat0[] and cvec0[]  ***  */
        memcpy(schf->dmat0, schf->dmat1, ns_sq * sizeof(real));
        memcpy(schf->cvec0, schf->cvec1, ns_sq * sizeof(realc));

        /*  ***  carlotta gkba schf shfmat  ***  */
        carlotta_gkba_schf_shfmat(params, mat, schf, schf->dmat1);

        /*  ***  set hmat[]  ***  */
        for (j = 0; j < ns; j++)
        {
            const int nsj = ns * j;
            for (i = 0; i < ns; i++)
            {
                if ((params->npart == 1) && (params->spin == 0))
                    schf->hmat[i + nsj] = mat->hmat[i + nsj];
                else
                    schf->hmat[i + nsj] =
                        mat->hmat[i + nsj] + schf->shfmat[i + nsj];
            }
        }

        setHmatForAdiabaticSwitching(params, mat);

        /*  ***  carlotta gkba lapack rcdiag  ***  */
        carlotta_gkba_lapack_rcdiag(ns, schf->hmat, schf->cval, schf->cvec1);

        /*  ***  carlotta gkba schf mu  ***  */
        schf->mu = carlotta_gkba_schf_mu(ns, npart, beta, schf->cval);

        /*  ***  set dmat1[]  ***  */
        for (j = 0; j < ns; j++)
        {
            const int nsj = ns * j;
            for (i = 0; i < ns; i++)
            {
                real su = 0.0;
                for (k = 0; k < ns; k++)
                    su += (1.0 - alpha) * carlotta_gkba_schf_distrib(
                                              beta, schf->cval[k], schf->mu) *
                          creal(schf->cvec1[i + ns * k]) *
                          creal(schf->cvec1[j + ns * k]);
                for (k = 0; k < ns; k++)
                    su += alpha * carlotta_gkba_schf_distrib(
                                      beta, schf->cval[k], schf->mu) *
                          creal(schf->cvec0[i + ns * k]) *
                          creal(schf->cvec0[j + ns * k]);
                schf->dmat1[i + nsj] = su;
            }
        }

        /*  ***  carlotta gkba schf observables  ***  */
        carlotta_gkba_schf_observables(params, mat, schf);

        /*  ***  increase  ***  */
        nit++;

        carlotta_gkba_schf_display(schf, nit);

    } while ((schf->acc) > acc_req);
}
